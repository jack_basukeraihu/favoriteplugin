package org.bitbucket.jack_basukeraihu;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class Favorite extends JavaPlugin {

	public void onEnable() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new BukkitListener(this), this);
		BukkitListener.NewFavo(getConfig().getInt("Timer"));
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new CheckTask(), 0L, 20L);
		saveDefaultConfig();
	}

	public void onDisable() {

	}

	static String pex = ChatColor.GOLD + "[FavoritePlugin] ";
	HashMap<Player, Integer> favorite = new HashMap<Player, Integer>();
	HashMap<Player, Integer> unfavorite = new HashMap<Player, Integer>();

	HashMap<Player, Integer> Newfavorite = new HashMap<Player, Integer>();

	@SuppressWarnings("rawtypes")
	HashMap<Player, ArrayList> favoPlayer = new HashMap<Player, ArrayList>();
	@SuppressWarnings("rawtypes")
	HashMap<Player, ArrayList> unfavoPlayer = new HashMap<Player, ArrayList>();

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("favorite")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "コンソールからは実行できません");
				return true;
			}
			Player p = (Player) sender;
			if (args.length != 1) {
				p.sendMessage(pex + "コマンドの形式が違います /" + cmd.getName()+ " <PlayerID>");
				return true;
			}
			Player target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				sender.sendMessage(pex + ChatColor.GREEN + args[0]+ ChatColor.RED + " というプレイヤーは見つかりません！");
				return true;
			}
			if (favoPlayer.get(p) == null){
				ArrayList<String> put = new ArrayList();
				favoPlayer.put(p, put);
			}
			if (favoPlayer.get(p).contains(target.getName())) {
				p.sendMessage(pex + ChatColor.RED+ "同じプレイヤーに一日複数回ふぁぼすることは出来ません");
				return true;
			}
			if (p == target) {
				p.sendMessage(pex + ChatColor.RED + "自分をふぁぼする事は出来ません");
				return true;
			}
			if (favorite.get(p) == null){
				favorite.put(p,0);
			}
			if (favorite.get(p) > getConfig().getInt("FavoriteLimit")){
				p.sendMessage(pex + ChatColor.RED+ "一日にふぁぼ出来る限界数に達しているため、ふぁぼは出来ません");
				return true;
			}
			int count = getConfig().getInt("PlayerData." + target.getUniqueId());
			getConfig().set("PlayerData." + target.getUniqueId(), count + 1);
			saveConfig();
			p.sendMessage(pex + ChatColor.GREEN + target.getName()+ "にふぁぼを行いました!");
			favorite.put(p, favorite.get(p) + 1);
			if (Newfavorite.get(target) == null){
				Newfavorite.put(target,0);
			}
			Newfavorite.put(target, Newfavorite.get(target) + 1);
			favoPlayer.get(p).add(target.getName());
		}

		if (cmd.getName().equalsIgnoreCase("unfavorite")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "コンソールからは実行できません");
				return true;
			}
			Player p = (Player) sender;
			if (args.length != 1) {
				p.sendMessage(pex + "コマンドの形式が違います /" + cmd.getName()+ " <PlayerID>");
				return true;
			}
			Player target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				sender.sendMessage(pex + ChatColor.GREEN + args[0]+ ChatColor.RED + " というプレイヤーは見つかりません！");
				return true;
			}
			if (unfavoPlayer.get(p) == null){
				ArrayList<String> put = new ArrayList();
				unfavoPlayer.put(p, put);
			}
			if (unfavoPlayer.get(p).contains(target.getName())){
				p.sendMessage(pex + ChatColor.RED+ "同じプレイヤーに一日複数回あんふぁぼすることは出来ません");
				return true;
			}
			if (p == target) {
				p.sendMessage(pex + ChatColor.RED + "自分をあんふぁぼする事は出来ません");
				return true;
			}
			if (unfavorite.get(p) == null){
				unfavorite.put(p,0);
			}
			if (unfavorite.get(p) > getConfig().getInt("UnFavoriteLimit")){
				p.sendMessage(pex + ChatColor.RED+ "一日にあんふぁぼ出来る限界数に達しているため、あんふぁぼは出来ません");
				return true;
			}
			int count = getConfig().getInt("PlayerData." + target.getUniqueId());
			if (count == 0) {
				p.sendMessage(pex + ChatColor.GREEN + target.getName()+ "にあんふぁぼを行いました");
				return true;
			}
			getConfig().set("PlayerData." + target.getUniqueId(), count - 1);
			saveConfig();
			p.sendMessage(pex + ChatColor.GREEN + target.getName()+ "にあんふぁぼを行いました");
			unfavorite.put(p, unfavorite.get(p) + 1);
			unfavoPlayer.get(p).add(target.getName());
		}

		if (cmd.getName().equalsIgnoreCase("favoritestats")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "コンソールからは実行できません");
				return true;
			}
			Player p = (Player) sender;
			int count = getConfig().getInt("PlayerData." + p.getUniqueId());
			p.sendMessage(pex + ChatColor.GREEN + p.getName() + "の評価値は "+ ChatColor.YELLOW + count + ChatColor.GREEN + " です");
		}
		return false;
	}
}
