package org.bitbucket.jack_basukeraihu;

import java.util.Calendar;

public class CheckTask implements Runnable {

	@Override
	public void run() {
		Calendar calendar = Calendar.getInstance();
		if ((calendar.get(Calendar.HOUR_OF_DAY) == 0) && (calendar.get(Calendar.MINUTE) == 0) && (calendar.get(Calendar.SECOND) == 0))
		{
			BukkitListener.plugin.favorite.clear();
			BukkitListener.plugin.unfavorite.clear();
			BukkitListener.plugin.favoPlayer.clear();
			BukkitListener.plugin.unfavoPlayer.clear();
		}
	}
}
