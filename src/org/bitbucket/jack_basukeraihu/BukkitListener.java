package org.bitbucket.jack_basukeraihu;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class BukkitListener implements Listener {

	public static Favorite plugin;

	public BukkitListener(Favorite instance) {
		plugin = instance;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if (!plugin.getConfig().contains("PlayerData."+p.getUniqueId())){
			plugin.getConfig().set("PlayerData."+p.getUniqueId(), plugin.getConfig().getInt("DefaultFavorite"));
			plugin.saveConfig();
		}
	}

	  public static void NewFavo(final int step)
 	  {
 	    if (step > 0)
 	    {
 	    	Bukkit.getServer().getScheduler().runTaskLater(plugin, new Runnable()
 	      {
 	        public void run()
 	        {
 	        	NewFavo(step - 1);
 	        }
 	      }, 20L);
 	    }
 	    else
 	    {
 	    	for (Player pl : Bukkit.getOnlinePlayers()){
 	    		int count;
 	    		if (plugin.Newfavorite.get(pl) == null){
 	    			count = 0;
 	    		} else{
 	    			count = plugin.Newfavorite.get(pl);
 	    		}

 	    		pl.sendMessage(Favorite.pex+ChatColor.GREEN+"貴方は新しく "+ChatColor.AQUA+count+ChatColor.GREEN+" 回ふぁぼられました");
 	    	}
 	    	plugin.Newfavorite.clear();
 	    	NewFavo(plugin.getConfig().getInt("Timer"));

 	    }
 	  }

}
